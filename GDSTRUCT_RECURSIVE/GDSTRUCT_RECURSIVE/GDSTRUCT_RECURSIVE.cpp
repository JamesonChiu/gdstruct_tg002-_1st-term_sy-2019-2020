// GDSTRUCT_RECURSIVE.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

void recursivePrintReverse(int number)
{
	if (number <= 0)
	{
		return;
	}

	for (int i = 0; i < number; i++)
		cout << number << " ";

	recursivePrintReverse(number - 1);



}


void recursiveDigitSum(int number, int sum)
{


	sum += number % 10;
	number /= 10;

	if (number <= 0)
	{
		cout << sum;
		return;
	}

	recursiveDigitSum(number, sum);

}

void recursiveFibonnaci(int number, int term1, int term2, int nextTerm)
{
	cout << nextTerm << "  ";
	nextTerm = term1 + term2;
	term1 = term2;
	term2 = nextTerm;

	if (number <= 0)
	{

		return;
	}

	recursiveFibonnaci(number - 1, term1, term2, nextTerm);
}

void recursivePrimeNumber(int number, int num)
{
	if (number % num == 0)
	{
		cout << "Not prime Number";
		return;
	}
	if (num * num > number)
	{
		cout << "Prime Number";
		return;
	}

	recursivePrimeNumber(number, num + 1);
}

int main()
{
	int sum = 0;
	int term1 = 0;
	int term2 = 1;
	int nextTerm = 1;
	int num = 2;
	bool prime = false;

	int number;
// Number 1
	cout << "Input Number" << endl;
	cin >> number;

	recursiveDigitSum(number, sum);
	cout << endl;
	system("pause");
	system("cls");
// Number 2
	cout << "Input Number" << endl;
	cin >> number;

	cout << "Overall Fibonnaci Sequence " << endl;
	recursiveFibonnaci(number, term1, term2, nextTerm);
	cout << endl;
	system("pause");
	system("cls");
// Number 3
	cout << "Input Number" << endl;
	cin >> number;

	recursivePrimeNumber(number, num);
	cout << endl;
	system("pause");
	system("cls");

	
}

