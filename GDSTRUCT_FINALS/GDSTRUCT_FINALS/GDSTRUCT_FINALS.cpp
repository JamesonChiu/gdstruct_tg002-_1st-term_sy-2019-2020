// GDSTRUCT_FINALS.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>
#include "Stack.h"
#include "Queue.h"
using namespace std;

int main()
{
	int size;
	int choice;
	bool x = true;

	cout << "Input Size: ";
	cin >> size;

	Stack<int> stack(size);
	Queue<int> queue(size);


	while (x)
	{
		cout << "What will you do?" << endl;
		cout << "[1] Push Number" << endl;
		cout << "[2] Pop Number" << endl;
		cout << "[3] Print and delete" << endl;
		cin >> choice;

		if (choice == 1)
		{
			int number;
			cout << "Input Number: " << endl;
			cin >> number;
			stack.pushBack(number);
			queue.pushFront(number);
		}
		else if (choice == 2)
		{
			cout << "Number Pop" << endl;
			stack.pop();
			queue.pop();
			_getch();
			system("cls");
		}
		else if (choice == 3)
		{
			//PRINT
			cout << "Stack: " << endl;
			for (int i = stack.getSize() - 1; i <= stack.getSize(); i--)
			{

				if (i < 0)
				{
					break;
				}

				cout << stack[i] << endl;
			}
			
			cout << endl;
			cout << "Queue: " << endl;
			
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << endl;
			}

			_getch();
			system("cls");

			//REMOVE
			stack.clear();
			queue.clear();
		}
		else
		{
			cout << "Invalid Command" << endl;
		}

		if (2 > choice)
		{
			cout << endl << "Top Stack Value: " << stack[stack.getSize() - 1];
			cout << endl << "Top Queue Value: " << queue[0];
			_getch();
			system("cls");
		}
		
	}

}


//EXTRA
/*stack.pushBack(5); //Back
	stack.pushBack(3);
	stack.pushBack(1);
	stack.pushBack(2); //Front

	//cout << stack.getSize() << endl;

	cout << "Stack: " << endl;
	for (int i = stack.getSize() - 1; i <= stack.getSize(); i--)
	{

		if (i < 0)
		{
			break;
		}

		cout << stack[i] << " ";
		_getch();
	}

	cout << endl << "Top Value: " << stack[0];


	cout << endl;

	queue.pushFront(5); //Back
	queue.pushFront(3);
	queue.pushFront(1);
	queue.pushFront(2); //Front

	cout << "Queue: " << endl;

	for (int i = 0; i < queue.getSize(); i++)
	{
		cout << queue[i] << " ";
		_getch();
	}

	cout << endl << "Top Value: " << queue[queue.getSize() - 1];

	cout << "POP" << endl;
	stack.pop();
	queue.pop();
	cout << endl << "Top Value: " << stack[0];
	cout << endl << "Top Value: " << queue[queue.getSize() - 1];*/



	/*for (int i = 0; i < size; i++)
		{
			int rng = rand() % 101;
			stack.push(rng);
			queue.push(rng);
		}*/
/*for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		stack.push(rng);
		queue.push(rng);
	}*/

	/*for (int i = 0; i < stack.getSize(); i++)
	{
		cout << stack[i] << " ";
	}*/

	/*cout << endl;

	queue.pushFront(0);
	queue.pushFront(1);

	for (int i = 0; i < queue.getSize(); i++)
	{
		cout << queue[i] << " ";
	}

	cout << endl;

	queue.pop();

	for (int i = 0; i < queue.getSize(); i++)
	{
		cout << queue[i] << " ";
	}*/

	/*//assert(mArray != NULL && index <= mMaxSize);

for (int i = 0; i < mMaxSize - 1; i++)
	mArray[i] = mArray[i + 1];

mMaxSize--;

if (mNumElements >= mMaxSize)
mNumElements = mMaxSize;*/