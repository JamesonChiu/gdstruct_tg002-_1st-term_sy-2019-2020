#include "pch.h"
#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));

	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	while (true)
	{
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << "\n\nWhat do you want to do?" << endl;
		cout << "1.) Remove Element" << endl;
		cout << "2.) Search Element" << endl;
		cout << "3.) Generate Element" << endl;

		int ans;
		cin >> ans;
		if (ans == 1)
		{
			if (unordered.getSize() == 0 || ordered.getSize()==0)
			{
				cout << "There's nothing to delete" << endl;
				system("pause");
				system("cls");
			}
			else
			{
				int index;
				cout << "What Index?" << endl;
				cin >> index;
				unordered.remove(index);
				ordered.remove(index);
				cout << "Element Removed" << endl;
				system("pause");
				system("cls");
			}
			
			
		}
		else if (ans == 2)
		{
			if (unordered.getSize() == 0 || ordered.getSize()==0)
			{
				cout << "There's nothing to search" << endl;
				system("pause");
				system("cls");

			}
			else
			{
				cout << "\n\nEnter number to search: ";
				int input;
				cin >> input;
				cout << endl;

				cout << "Unordered Array(Linear Search):\n";
				int result = unordered.linearSearch(input);
				if (result >= 0)
					cout << input << " was found at index " << result << ".\n";
				else
					cout << input << " not found." << endl;

				cout << "Ordered Array(Binary Search):\n";
				result = ordered.binarySearch(input);
				if (result >= 0)
					cout << input << " was found at index " << result << ".\n";
				else
					cout << input << " not found." << endl;
				system("pause");
				system("cls");
			}
		}
		else if (ans == 3)
		{
			int rng = rand() % 101;
			unordered.push(rng);
			ordered.push(rng);
			cout << "Generate Element" << endl;
			system("pause");
			system("cls");
		}
		else
		{
			cout << "ERROR" << endl;
			system("pause");
			system("cls");
		}
		
	}
	
}